package com.alit.rebootapp;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class TimerActivity extends AppCompatActivity {

	protected static Timer timer = new Timer();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		startTimer();
	}

	protected void startTimer() {
		long dayInMillis = TimeUnit.DAYS.toMillis(1);
		timer.scheduleAtFixedRate(new RebootTimerTask(), dayInMillis, dayInMillis);
	}

	protected class RebootTimerTask extends TimerTask
	{
		public void run()
		{
			final PackageManager packageManager = getPackageManager();
			final Intent intent = packageManager.getLaunchIntentForPackage(getPackageName());
			if(intent != null) {
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
				System.exit(0);
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		timer.cancel();
	}
}
