package com.alit.rebootapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import java.util.Calendar;

public class AlarmManagerActivity extends AppCompatActivity {

	protected static final String RESET_FILTER = "com.alit.rebootapp.resetfilter";

	protected BroadcastReceiver resetReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			final PackageManager packageManager = context.getPackageManager();
			final Intent restartIntent = packageManager.getLaunchIntentForPackage(context.getPackageName());
			if (restartIntent != null) {
				restartIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				context.startActivity(restartIntent);

				AlarmManagerActivity.this.finish();
			}
		}
	};

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		setRestartAlarm();

	}

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(resetReceiver, new IntentFilter(RESET_FILTER));
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(resetReceiver);
	}

	protected void setRestartAlarm() {
		AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		if (alarmManager == null) {
			return;
		}
		final Intent intent = new Intent(RESET_FILTER);
		final PendingIntent restartPendingIntent = PendingIntent
				.getBroadcast(this, 1, intent, PendingIntent.FLAG_ONE_SHOT);

		alarmManager.set(AlarmManager.RTC_WAKEUP, getRestartTimeInMillis(), restartPendingIntent);
	}

	protected long getRestartTimeInMillis() {
		final Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 16);
		calendar.set(Calendar.MINUTE, 25);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		// because of this, the restarting is triggered in the following day
		calendar.add(Calendar.DAY_OF_MONTH,1);
		return calendar.getTimeInMillis();
	}
}
