package com.alit.rebootapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import java.util.Calendar;

public class CountDownTimerActivity extends AppCompatActivity {

	protected CountDownTimer countDownTimer;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		setRestartAlarm();
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (countDownTimer != null) {
			countDownTimer.cancel();
		}
	}

	protected void setRestartAlarm() {
		long timeForNextDawnInMillis = getTimeForNextDawn();
		countDownTimer = new CountDownTimer(timeForNextDawnInMillis, timeForNextDawnInMillis) {

			public void onTick(long millisUntilFinished) {

			}

			public void onFinish() {
				finishApp(getApplicationContext());
			}
		};
		countDownTimer.start();
	}

	protected long getTimeForNextDawn() {
		final Calendar nextDawnTime = Calendar.getInstance();
		nextDawnTime.set(Calendar.HOUR_OF_DAY, 4);
		nextDawnTime.set(Calendar.MINUTE, 30);
		nextDawnTime.set(Calendar.SECOND, 0);
		// because of this, the restarting is triggered in the following day
		nextDawnTime.add(Calendar.DAY_OF_MONTH, 1);

		return nextDawnTime.getTimeInMillis() - System.currentTimeMillis();
	}

	protected void finishApp(final Context context) {
		final PackageManager packageManager = context.getPackageManager();
		final Intent restartIntent = packageManager.getLaunchIntentForPackage(context.getPackageName());
		if (restartIntent != null) {
			restartIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			context.startActivity(restartIntent);

			CountDownTimerActivity.this.finish();
		}
	}
}
